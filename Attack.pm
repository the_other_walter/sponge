package Attack;
use Item;
use strict;
use Switch;
our @ISA = qw(Item);

sub new
{
  my ($class) = @_;
  my $self = $class->SUPER::new($_[1], $_[2],$_[3],$_[4], $_[5]);
  bless $self, $class;
  return $self;
}

