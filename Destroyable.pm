package Destroyable;
use Item;
use Maze;
use strict;
use Shrapnel;
use Switch;
our @ISA = qw(Item);

sub new
{
  my ($class) = @_;
  my $self = $class->SUPER::new($_[1], $_[2],$_[3],$_[4], $_[5]);
  $self->{_hp} = $_[6];
  $self->{_explosion_flash} = $_[7];
  $self->{_horizontal_force} = $_[8];
  $self->{_vertical_force} = $_[9];
  bless $self, $class;
  return $self;
}

sub flash
{
  my $self = shift;
  my $map = shift;
  my $caster_x = shift;
  my $caster_y = shift;
  my $flash = new Item("Flash", $self->{_explosion_flash});
  $map->set_particle_at($caster_x, $caster_y, $flash);
  # Tell the program to destroy this eventually.
  $flash->{_die_chance} = 0.01;
}

sub be_damaged
{
  my $self = shift;
  my $map = shift;
  my $caster_x = shift;
  my $caster_y = shift;
  my $damage = shift;
  # Take damage.
  $self->{_hp} -= $damage;
  # Flash.
  $self->flash($map, $caster_x, $caster_y);
  # Die if needed.
  $self->explode($map, $caster_x, $caster_y) if($self->{_hp} <= 0);
}

sub explode
{
  my $self = shift;
  my $map = shift;
  my $caster_x = shift;
  my $caster_y = shift;
  # For each tile in the image, create a projectile.
  my ($x, $y) = ($caster_x+$self->get_x_offset,
                 $caster_y+$self->get_y_offset);
  my $i = $x;
  foreach(split //, $self->get_image)
  {
    if($_ eq "\n") {$i = $x; $y++}
    else
    {
      my $shrap = new Shrapnel("Shrap", $_);
      $shrap->init_vel($self->{_horizontal_force}, $self->{_vertical_force});
      $map->set_particle_at($i, $y, $shrap);
      $i++;
    }
  }
  # Flash.
  $self->flash($map, $caster_x, $caster_y);
  # Die.
  $map->clear_item_at($caster_x, $caster_y);
}
1;
