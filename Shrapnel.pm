package Shrapnel;
use Item;
use Maze;
use Math::Trig ':pi';
use strict;
use Switch;
our @ISA = qw(Item);

sub new
{
  my ($class) = @_;
  my $self = $class->SUPER::new($_[1], $_[2], "", "", "");
  $self->{_x_vel} = $_[3];
  $self->{_y_vel} = $_[4];
  $self->{_z_vel} = $_[5];
  # Shrapnel vanishes after a while.
  $self->{_die_chance} = 0.1;
  bless $self, $class;
  return $self;
}

sub init_vel
{
  my $self = shift;
  my $horizontal = shift;
  my $vertical = shift;
  # Pick magnetudes.
  $self->{_z_vel} = -int rand($vertical);
  my $horizontal = rand($horizontal);
  # Pick an angle to fly at.
  my $angle = rand 2*pi;
  # Translate polar to rectangular.
  $self->{_x_vel} = int($horizontal * cos $angle);
  $self->{_y_vel} = int($horizontal * sin $angle);
}

sub update_coords
{
  my $self = shift;
  my $map = shift;
  my $caster_x = shift;
  my $caster_y = shift;
  my $next_x = $caster_x + $self->{_x_vel};
  my $next_y = $caster_y + $self->{_y_vel} + $self->{_z_vel};
  # Accelerate downwards.
  $self->{_z_vel}++;
  # Move.
  if(!defined $map->get_particle_at($next_x, $next_y))
  {
    $map->set_particle_at($next_x, $next_y, $self);
    $map->clear_particle_at($caster_x, $caster_y);
  }
}
