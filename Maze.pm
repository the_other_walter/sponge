# By Walter Brown.  Happy Birthday Jacob!
package Maze;
use Display;
use strict;
use warnings;

use constant DRAW_CENTER => 0;

sub new
{
  my $class = shift;
  my $self = {};
  bless $self, $class;
  return $self;
}

sub get_item_at
{
  my ($self, $x, $y) = @_;
  return $self->{"$x,$y"} if defined $self->{"$x,$y"};
  return undef;
}

sub get_particle_at
{
  my ($self, $x, $y) = @_;
  return $self->{"P$x,$y"} if defined $self->{"P$x,$y"};
  return undef;
}

sub clear_item_at
{
  my ($self, $x, $y) = @_;
  delete $self->{"$x,$y"};
}

sub set_item_at
{
  my ($self, $x, $y, $item) = @_;
  if(defined $item) {$self->{"$x,$y"} = $item}
  else {undef $self->{"$x,$y"}}
}

sub set_particle_at
{
  my ($self, $x, $y, $item) = @_;
  if(defined $item) {$self->{"P$x,$y"} = $item}
  else {undef $self->{"P$x,$y"}}
}

sub clear_particle_at
{
  my ($self, $x, $y) = @_;
  delete $self->{"P$x,$y"};
}

sub draw_image
{
  my ($self, $center_x, $center_y, $radius, $screen_x, $screen_y) = @_;
  for(my $y = $center_y - $radius; $y < $center_y + $radius; $y++)
  {
    for(my $x = $center_x + $radius; $x > $center_x - $radius; $x--)
    {
      # If there's someone at this tile...
      if(defined $self->{"$x,$y"})
      {
        my $item = $self->{"$x,$y"};

        # Draw item.
        set_pixel($x+$item->get_x_offset+$screen_x-$center_x+$radius, 
        $y+$item->get_y_offset+$screen_y-$center_y+$radius, $item->get_image);
        set_pixel($x+$screen_x-$center_x+$radius, 
        $y+$screen_y-$center_y+$radius, "X") if DRAW_CENTER;
      }
    }
    for(my $x = $center_x + $radius; $x > $center_x - $radius; $x--)
    {
      # Draw particles over.
      if(defined $self->{"P$x,$y"})
      {
        my $item = $self->{"P$x,$y"};

        # Draw item.
        set_pixel($x+$item->get_x_offset+$screen_x-$center_x+$radius, 
        $y+$item->get_y_offset+$screen_y-$center_y+$radius, $item->get_image);
        set_pixel($x+$screen_x-$center_x+$radius, 
        $y+$screen_y-$center_y+$radius, "P") if DRAW_CENTER;
      }
    }
  }
}
1;
