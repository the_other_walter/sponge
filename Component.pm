package Component;
use Item;
use strict;
use Switch;
our @ISA = qw(Item);

sub new
{
  my ($class) = @_;
  my $self = $class->SUPER::new($_[1], "-|-\n","-","-","");
  bless $self, $class;
  return $self;
}

sub update_image
{
  my $self = shift;
  $self->{_down} = "$self->{_left}|$self->{_right}";
}

sub set_left
{
  my $self = shift;
  my $component_string = shift;
  $self->{_left} = $component_string;
  $self->update_image;
}

sub set_right
{
  my $self = shift;
  my $component_string = shift;
  $self->{_right} = $component_string;
  $self->update_image;
}

# RW means rocket on left, ward on right.
sub set_type
{
  my $self = shift;
  my $type = shift;
  # Set type.
  if($type =~ /^(.)(.)$/)
  {
    $self->set_left($1);
    $self->set_right($2);
  }
  # If malformed, purge type.
  else
  {
    $self->set_left("-");
    $self->set_right("-");
  }
}

sub get_left
{
  my $self = shift;
  return $self->{_left};
}

sub get_right
{
  my $self = shift;
  return $self->{_right};
}

sub is_empty
{
  my $self = shift;
  return ($self->get_left() eq "-") && ($self->get_right() eq "-");
}
