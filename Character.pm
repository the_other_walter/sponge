package Character;
use Item;
use strict;
use Switch;
our @ISA = qw(Item);

sub new
{
  my ($class) = @_;
  my $self = $class->SUPER::new($_[1], $_[2], $_[3], $_[4], $_[5]);
  $self->{_inventory} = "--"; # String.
  bless $self, $class;
  return $self;
}

sub set_left
{
  my $self           = shift;
  my $component_char = shift;
  substr($self->{_inventory}, 0, 1)  = $component_char;
}

sub set_right
{
  my $self           = shift;
  my $component_char = shift;
  substr($self->{_inventory}, 1, 1)  = $component_char;
}

sub get_left
{
  my $self = shift;
  return substr($self->{_inventory}, 0, 1);
}

sub get_right
{
  my $self = shift;
  return substr($self->{_inventory}, 1, 1);
}

sub get_type
{
  my $self = shift;
  return $self->get_left.$self->get_right;
}
