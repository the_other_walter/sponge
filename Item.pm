# By Walter Brown.  Happy Birthday Jacob!
package Item;
use List::Util qw(max);
use strict;
use Switch;
use constant {DOWN => 0, LEFT => 1, RIGHT => 2, UP => 3};

sub new
{
  my $class = shift;
  my $self = 
  {
    _name      => shift,
    _down      => shift,
    _left      => shift,
    _right     => shift,
    _up        => shift,
    _direction => DOWN
  };
  bless $self, $class;
  ($self->{_x_offset_down},  $self->{_y_offset_down}) =
                                       $self->calculate_offset($self->{_down});
  ($self->{_x_offset_left},  $self->{_y_offset_left}) =
                                       $self->calculate_offset($self->{_left});
  ($self->{_x_offset_right}, $self->{_y_offset_right}) =
                                       $self->calculate_offset($self->{_right});
  ($self->{_x_offset_up},    $self->{_y_offset_up}) =
                                       $self->calculate_offset($self->{_up});
  return $self;
}

sub get_image
{
  my $self = shift;
  switch($self->{_direction})
  {
    case DOWN  {return $self->{_down}}
    case LEFT  {return $self->{_left}}
    case RIGHT {return $self->{_right}}
    case UP    {return $self->{_up}}
  }
}

sub get_x_offset
{
  my $self = shift;
  switch($self->{_direction})
  {
    case DOWN  {return $self->{_x_offset_down}}
    case LEFT  {return $self->{_x_offset_left}}
    case RIGHT {return $self->{_x_offset_right}}
    case UP    {return $self->{_x_offset_up}}
  }
}

sub get_y_offset
{
  my $self = shift;
  switch($self->{_direction})
  {
    case DOWN  {return $self->{_y_offset_down}}
    case LEFT  {return $self->{_y_offset_left}}
    case RIGHT {return $self->{_y_offset_right}}
    case UP    {return $self->{_y_offset_up}}
  }
}

sub turn_down
{
  my ($self) = @_;
  $self->{_direction} = DOWN;
}
sub turn_left
{
  my ($self) = @_;
  $self->{_direction} = LEFT;
}
sub turn_right
{
  my ($self) = @_;
  $self->{_direction} = RIGHT;
}
sub turn_up
{
  my ($self) = @_;
  $self->{_direction} = UP;
}

sub match_direction
{
  my ($self, $other) = @_;
  $self->{_direction} = $other->{_direction};
}

# Calulates the x,y offset of upper-left corner of the image.
sub calculate_offset
{
  my $self = shift;
  my $image = shift;
  my $x = 0; # For now, will sum the widths of the rows.
  my $y = -1; # For now, will count the number of columns.
  my $temp_x = 0;
  foreach(split //, $image)
  {
    if($_ eq "\n")
    {
      $x = max($temp_x, $x);
      $temp_x = 0;
      $y++;
    }
    else
    {
      $temp_x++;
    }
  }
  # Center.
  $x = int($x / 2);
  return (-$x, -$y);
}

# Translates the direction of the item to a pair of coordinates being faced.
sub front
{
  my $self = shift;
  switch($self->{_direction})
  {
    case DOWN  {return ( 0, 1)}
    case LEFT  {return (-1, 0)}
    case RIGHT {return ( 1, 0)}
    case UP    {return ( 0,-1)}
  }
}

1;
