package Bullet;
use Destroyable;
use Item;
use Maze;
use strict;
use Switch;
our @ISA = qw(Item);

sub new
{
  my ($class) = @_;
  my $self = $class->SUPER::new($_[1], $_[2],$_[3],$_[4], $_[5]);
  $self->{_damage} = $_[6];
  $self->{_range} = $_[7];
  $self->{_explosion_flash} = $_[8];
  bless $self, $class;
  return $self;
}

sub flash
{
  my $self = shift;
  my $map = shift;
  my $caster_x = shift;
  my $caster_y = shift;
  my $flash = new Item("Flash", $self->{_explosion_flash});
  $map->set_particle_at($caster_x, $caster_y, $flash);
  # Tell the program to destroy this eventually.
  $flash->{_die_chance} = 0.01;
}

sub end
{
  my $self = shift;
  my $map = shift;
  my $caster_x = shift;
  my $caster_y = shift;
  $self->flash($map, $caster_x, $caster_y);
  $map->clear_item_at($caster_x, $caster_y);
}

sub update_coords
{
  my $self = shift;
  my $map = shift;
  my $caster_x = shift;
  my $caster_y = shift;
  my ($next_x, $next_y) = $self->front;
  $next_x += $caster_x;
  $next_y += $caster_y;
  # If you've hit a target, detonate!
  if(defined $map->get_item_at($next_x, $next_y))
  {
    # Damage target if able.
    if($map->get_item_at($next_x, $next_y)->isa('Destroyable'))
    {
      $map->get_item_at($next_x, $next_y)->be_damaged($map, $next_x, $next_y,
                                                      $self->{_damage});
    }
    $self->end($map, $caster_x, $caster_y);
    return;
  }
  # Move.
  else
  {
    $map->set_item_at($next_x, $next_y, $self);
    $map->clear_item_at($caster_x, $caster_y);
    # Maybe reach max range.
    $self->{_range}--;
    if($self->{_range} <= 0)
    {
      $self->end($map, $next_x, $next_y);
      return;
    }
  }
}
