package Monster;
use Destroyable;
use Item;
use Maze;
use strict;
use Switch;
our @ISA = qw(Destroyable);

our $kill_count = 0;

sub new
{
  my ($class) = @_;
  my $self = $class->SUPER::new($_[1], $_[2],$_[3],$_[4], $_[5], $_[6], $_[7],
                                $_[8], $_[9]);
  $self->{_movement} = $_[10];
  $self->{_attack} = $_[11];
  bless $self, $class;
  return $self;
}

sub next_position
{
  my $self = shift;
  my $now_x = shift;
  my $now_y = shift;
  my $targ_x = shift;
  my $targ_y = shift;
  my $map = shift;
  my @result = ($now_x, $now_y);
  # If you aren't on the x you want, you have a chance of changing x.
  if($now_x != $targ_x && rand() < $self->{_movement})
  {
    if($now_x < $targ_x) {$result[0] = $now_x+1}
    else                 {$result[0] = $now_x-1}
  }
  #same for y.
  if($now_y != $targ_y && rand() < $self->{_movement})
  {
    if($now_y < $targ_y) {$result[1] = $now_y+1}
    else                 {$result[1] = $now_y-1}
  }
  # If spot is not clear, stay.
  return ($now_x, $now_y) if(defined $map->get_item_at(@result));
  # If clear, return calculated result. 
  return @result;
}

sub update_coords
{
  my $self =  shift;
  my $map = shift;
  my $my_x = shift;
  my $my_y = shift;
  my $target_x = shift;
  my $target_y = shift;
  my ($next_x, $next_y) =
               $self->next_position($my_x, $my_y, $target_x, $target_y, $map);
  # Move.
  $map->clear_item_at($my_x, $my_y);
  $map->set_item_at($next_x, $next_y, $self);

  # Turn.
  if($my_x < $next_x) { $self->turn_right };
  if($my_x > $next_x) { $self->turn_left };
  if($my_y > $next_y) { $self->turn_up };
  if($my_y < $next_y) { $self->turn_down };

  # Observe nearby objects.
  for(my $x_off = -1; $x_off < 2; $x_off++)
  {
    for(my $y_off = -1; $y_off < 2; $y_off++)
    {
      # Don't see self.
      if($x_off != $y_off)
      {
        my $damageable = $map->get_item_at($next_x+$x_off, $next_y+$y_off);
        # Only care about objects.
        next if !defined $damageable;
        # Don't kill monsters.
        next if $damageable->isa("Monster");
        # Detonate wards!
        if($damageable->{_name} eq "Ward0")
        {
          $self->be_damaged($map, $next_x, $next_y, 1);
          $damageable->be_damaged($map, $next_x+$x_off, $next_y+$y_off, 1);
        }
        if($damageable->{_name} eq "Ward1")
        {
          $self->be_damaged($map, $next_x, $next_y, 5);
          $damageable->be_damaged($map, $next_x+$x_off, $next_y+$y_off, 1);
        }
        if(rand() < $self->{_attack})
        {
          # Deal damage.
          if($damageable->isa('Destroyable'))
          {
            $damageable->be_damaged($map, $next_x+$x_off, $next_y+$y_off, 1);
          }
        }
      }
    }
  }
}

sub explode
{
  # Call parent explode.
  shift->SUPER::explode(@_);
  # Increment kill count.
  $kill_count++;
}

1;
