#!/usr/bin/perl
# Happy Birthday Jacob!  From, Walter
use strict;
use warnings;
use Bullet;
use Display;
use Destroyable;
use HotKey;
use Character;
use Component;
use Item;
use Maze;
use Monster;
use List::Util qw(max min);
use Time::HiRes qw(usleep time);
use Shrapnel;
use Switch;
use constant TICK => 50000;
use constant MAP_DIM => 100;

# Sucks a whole file into a string.
sub file_to_string
{
  my $file_name = shift;
  local $/ = undef; # Make it so that files will be read as one big string.
  open FILE, $file_name or die "Couldn't open $file_name.";
  my $result = <FILE>;
  close FILE;
  return $result;
}

sub story
{
  set_screen file_to_string "art/story";
  draw_screen;
  STDIN->blocking(1); # Make is so that readkey will wait for input.
  readkey;            # Wait for input.
  menu();
}

sub help
{
  set_screen file_to_string "art/help";
  draw_screen;
  STDIN->blocking(1); # Make is so that readkey will wait for input.
  readkey;            # Wait for input.
  menu();
}

sub menu
{
  use constant CURSOR_RIGHT => 23;
  use constant CURSOR_BOTTOM => 22;
  set_screen file_to_string "art/menu";
  draw_screen;
  my $cursor = 11;
  my $win_muffin = 0;
  my @blips  = (-1) x CURSOR_BOTTOM;
  STDIN->blocking(0); # Make is so that readkey will never WAIT for input.
  # Stay in menu.
  for(;;)
  {
    # Easter egg... Detect when all xs are erased.
    if(!$win_muffin)
    {
      my $found_x = 0;
      for(my $i = 0; $i < CURSOR_BOTTOM*CURSOR_RIGHT; $i++)
      {
        if(get_pixel($i % CURSOR_RIGHT, int($i / CURSOR_RIGHT)) eq 'x')
        {
          $found_x = 1;
          last;
        }
      }
      if(!$found_x)
      {
        $win_muffin = 1;
        set_pixel(32, 16, "WIN MUFFINS!!!\n U R Ready!!!");
      }
    }
    my $in = readkey;
    if($in)
    {
      if($in eq 'w')
      {
        $cursor--;
      }
      if($in eq 's')
      {
        $cursor++;
      }
      # Clear and set row (Easter egg)
      if($in eq 'a')
      {
        clear_pixel($_, $cursor)for(0..CURSOR_RIGHT);
      }
      if($in eq 'd')
      {
        set_pixel($_, $cursor, 'x')for(0..CURSOR_RIGHT);
      }
      if($in eq "\n")
      {
        # Start game.
        game() if $cursor == 11;
        # Start game.
        help() if $cursor == 12;
        # Start game.
        story() if $cursor == 13;
        # Exit game.
        exit if $cursor == 14;
      }
      $cursor %= CURSOR_BOTTOM;
    }
    # Let blips slide back.
    for(my $i = 0; $i < scalar @blips; $i++)
    {
      # Skip the selected item.
      next if $i == $cursor;
      # Erase origional blip.
      clear_pixel($blips[$i], $i);
      # Move blip.
      $blips[$i] = max($blips[$i]-int(rand 5)-1, -1);
      # Draw in where the blip is now.
      set_pixel($blips[$i], $i, ">");
    }
    # Push up selected item.
    clear_pixel($blips[$cursor], $cursor);
    $blips[$cursor] = min($blips[$cursor]+int(rand 10)+1, CURSOR_RIGHT);
    set_pixel($blips[$cursor], $cursor, ">");
    draw_screen;
    usleep TICK;
  }
}

# Used to manage boxes.
sub lift
{
  my $map = shift;
  my $caster_x = shift;
  my $caster_y = shift;
  # Grab caster and box.
  my $caster = $map->get_item_at($caster_x,$caster_y);
  my ($box_x, $box_y) = $caster->front;
  $box_x += $caster_x;
  $box_y += $caster_y;
  my $box = $map->get_item_at($box_x,$box_y);
  # Bail if the box doesn't exist.
  if(!defined $box or !$box->isa('Component')) {return ($caster_x, $caster_y)}
  # If the box is empty, drop all your supplies in.
  if($box->is_empty)
  {
    $box->set_type($caster->get_type);
    $caster->set_left("-");
    $caster->set_right("-");
    return ($caster_x, $caster_y);
  }
  # If the box's left side is full, swap your left with it.
  if($box->get_left ne "-")
  {
    my $temp = $box->get_left;
    $box->set_left($caster->get_left);
    $caster->set_left($temp);
  }
  # Now the same for the right.
  if($box->get_right ne "-")
  {
    my $temp = $box->get_right;
    $box->set_right($caster->get_right);
    $caster->set_right($temp);
  }
  return ($caster_x, $caster_y);
}

# Destroyes whatever is directly in front of you.
sub slash
{
  my $map = shift;
  my $caster_x = shift;
  my $caster_y = shift;
  my $big = shift;
  # Identify target.
  my $caster = $map->get_item_at($caster_x, $caster_y);
  my ($target_x, $target_y) = $caster->front;
  $target_x += $caster_x;
  $target_y += $caster_y;
  # Damage.
  my $target = $map->get_item_at($target_x, $target_y);
  if(defined $target and $target->isa('Destroyable'))
  {
    if($big) {$target->be_damaged($map, $target_x, $target_y, 20)}
    else {$target->be_damaged($map, $target_x, $target_y, 4)}
  }
  # Put up slash animation.
  my $animation = new Item("Slash",
                                   file_to_string("art/slash/down"),
                                   file_to_string("art/slash/left"),
                                   file_to_string("art/slash/right"),
                                   file_to_string("art/slash/up"),
		          );
  $animation->match_direction($caster);
  $animation->{_die_chance} = 0.9;
  $map->set_particle_at($caster_x, $caster_y, $animation);
  # Pivot to a random position.
  my ($pivot_x, $pivot_y) = ($caster_x, $caster_y);
  switch(int(rand 5))
  {
    case 0 {$pivot_x++}
    case 1 {$pivot_x--}
    case 2 {$pivot_y++}
    case 3 {$pivot_y--}
  }
  if(!defined $map->get_item_at($pivot_x, $pivot_y))
  {
    $map->clear_item_at($caster_x, $caster_y);
    $map->set_item_at($pivot_x, $pivot_y, $caster);
    return ($pivot_x, $pivot_y);
  }
  # Drop another shash animation.
  $map->set_particle_at($pivot_x, $pivot_y, $animation);
  return ($caster_x, $caster_y);
}

# Launches a projectile forward.
sub rocket
{
  my $map = shift;
  my $caster_x = shift;
  my $caster_y = shift;
  my $big = shift;
  my $caster = $map->get_item_at($caster_x, $caster_y);
  # Recoil backwards.
  my ($back_x, $back_y) = $caster->front;
  $back_x = $caster_x-$back_x;
  $back_y = $caster_y-$back_y;
  if(defined $map->get_item_at($back_x, $back_y))
  {
    return ($caster_x, $caster_y);
  }
  $map->clear_item_at($caster_x, $caster_y);
  $map->set_item_at($back_x, $back_y, $caster);
  # Make rocket.
  my $damage;
  if($big) {$damage = 10}
  else {$damage = 2}
  my $rocket = new Bullet("Rocket",
                                   file_to_string("art/rocket/down"),
                                   file_to_string("art/rocket/left"),
                                   file_to_string("art/rocket/right"),
                                   file_to_string("art/rocket/up"),
                                   $damage, 5, file_to_string("art/props/smoke")
	                 );
  $rocket->match_direction($caster);
  $map->set_item_at($caster_x, $caster_y, $rocket);
  return ($back_x, $back_y);
}

# Places a destroyable barrier that will explode on enemies.
sub ward
{
  my $map = shift;
  my $caster_x = shift;
  my $caster_y = shift;
  my $big = shift;
  my $caster = $map->get_item_at($caster_x, $caster_y);
  # Figure out which tiles will have the blocks.
  my (@block_1, @block_2);
  my ($front_x, $front_y) = $caster->front;
  if($front_x != 0)
  {
    # Facing horizontal
    @block_1 = ($caster_x, $caster_y-1);
    @block_2 = ($caster_x, $caster_y+1);
  }
  else
  {
    # Facing verticle
    @block_1 = ($caster_x+1, $caster_y);
    @block_2 = ($caster_x-1, $caster_y);
  }
  # Place the wards.
  if(!defined $map->get_item_at($block_1[0], $block_1[1]))
  {
    my $ward = new Destroyable("Ward$big", file_to_string("art/ward"),"","",
                              "", 1, file_to_string("art/none"), 1, 10);
    $map->set_item_at($block_1[0], $block_1[1], $ward);
  }
  if(!defined $map->get_item_at($block_2[0], $block_2[1]))
  {
    my $ward = new Destroyable("Ward$big", file_to_string("art/ward"),"","",
                              "", 1, file_to_string("art/none"), 1, 1);
    $map->set_item_at($block_2[0], $block_2[1], $ward);
  }
  return ($caster_x, $caster_y);
}

END
{
  clear;
  switch(int rand 10)
  {
    case 0  {print "Game over."}
    case 1  {print "I'm done."}
    case 2  {print "Joy to the world.  Tis Jacob's birthday."}
    case 3  {print "Awww bacon beagles."}
    case 4  {print "Fiddle marts..."}
    case 5  {print "Ergle."}
    case 6  {print "Humf."}
    case 7  {print "Terminated.  With a capital T."}
    case 8  {print "Rocker-dash"};
    case 9  {print "Well you know what?  I Quit you too!"}
  }
  print "\n";
}

# Test to make sure maze works.
sub game
{
  our $kill_count;
  $Monster::kill_count = 0;
  my $timer = time + 60*5;  # Game will go for 5 minutes.
  my $m = new Maze;
  my $center_x = -1;
  my $center_y = -1;
  my $player_character = new Character(
                                       "sponge",
                                       file_to_string("art/sponge/down"),
                                       file_to_string("art/sponge/left"),
                                       file_to_string("art/sponge/right"),
                                       file_to_string("art/sponge/up")
				      );
  $m->set_item_at($center_x,$center_y,$player_character);
  # Randomly generate map.
  for(my $i = 0; $i < 20; $i++)
  {
    my $rock = new Item("rock", file_to_string("art/props/rock"));
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$rock);
  }
  for(my $i = 0; $i < 4; $i++)
  {
    my $lamp = new Destroyable("lamp", file_to_string("art/props/lamp"),"","",
                               "", 3, "+", 10, 2);
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$lamp);
  }
  for(my $i = 0; $i < 50; $i++)
  {
    my $grass = new Destroyable("grass", file_to_string("art/props/grass"),"",
                                "", "", 1, file_to_string("art/props/smoke"),
				1, 1);
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$grass);
  }
  for(my $i = 0; $i < 50; $i++)
  {
    my $tree = new Destroyable("tree", file_to_string("art/props/tree"),"",
                                "", "", 5, file_to_string("art/props/smoke"),
				10, 2);
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$tree);
  }
  for(my $i = 0; $i < 10; $i++)
  {
    my $tree = new Destroyable("tree", file_to_string("art/props/pine"),"",
                                "", "", 5, file_to_string("art/props/smoke"),
				10, 2);
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$tree);
  }
  for(my $i = 0; $i < 5; $i++)
  {
    my $tree = new Destroyable("tree", file_to_string("art/props/fallen_tree"),
                               "", "", "", 4, file_to_string("art/props/smoke"),
			       10, 2);
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$tree);
  }
  for(my $i = 0; $i < 2; $i++)
  {
    my $tree = new Destroyable("tree", file_to_string("art/props/bird_house"),
                               "", "", "", 8, file_to_string("art/props/smoke"),
			       20, 0);
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$tree);
  }
  for(my $i = 0; $i < 3; $i++)
  {
    my $tree = new Destroyable("tree", file_to_string("art/props/burned_tree"),
                               "", "", "", 2, file_to_string("art/props/smoke"),
			       10, 2);
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$tree);
  }
  for(my $i = 0; $i < 50; $i++)
  {
    my $tree = new Destroyable("mush", file_to_string("art/props/mushroom"),
                               "", "", "", 1, file_to_string("art/props/smoke"),
			       3, 8);
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$tree);
  }
  for(my $i = 0; $i < 50; $i++)
  {
    my $tree = new Destroyable("flower", file_to_string("art/props/flower"),
                               "", "", "", 1, file_to_string("art/props/smoke"),
			       2, 2);
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$tree);
  }

  # Add caches.
  for(0..3)
  {
    my $cache = new Component("Wand");
    $cache->set_type("W-");
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$cache);
    $cache = new Component("Wand");
    $cache->set_type("-W");
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$cache);
    $cache = new Component("Sword Rack");
    $cache->set_type("S-");
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$cache);
    $cache = new Component("Sword Rack");
    $cache->set_type("-S");
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$cache);
    $cache = new Component("Silo");
    $cache->set_type("R-");
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$cache);
    $cache = new Component("Silo");
    $cache->set_type("-R");
    $m->set_item_at(int(rand(MAP_DIM)),int(rand(MAP_DIM)),$cache);
  }

  # Set starting gear.
  $player_character->set_left ("S");

  STDIN->blocking(0); # Make is so that readkey will never WAIT for input.
  while(1)
  {
    drop_screen;
    set_pixel(0,0,"($center_x,$center_y)");
    set_pixel(0,1,$player_character->get_left."|".$player_character->get_right);
    set_pixel(0,2,"Score:  $Monster::kill_count");
    $m->draw_image($center_x, $center_y, 8, 10, 1);
    my $time_left = int($timer - time());
    set_pixel(0,3,"Time Remaining:  $time_left");
    # End if out of time.
    if($time_left == 0)
    {
      drop_screen;
      set_pixel(0,0,"You got an epic score of $Monster::kill_count!");
      draw_screen;
      usleep 1000000;
      set_pixel(0,1,"Press enter key to return to the menu!");
      draw_screen;
      STDIN->blocking(1);
      while(readkey ne "\n"){}; # Wait for return.
      menu();
    }
    draw_screen;
    # Update individual objects.
    foreach my $obj_name (keys %$m)
    {
      # Only objects are allowed in this. (this is a messy cover for a bug.)
      # Bug seems to be from killing tree with rocket from some angles.
      if(!UNIVERSAL::can($m->{$obj_name}, 'isa'))
      {
        delete $m->{$obj_name};
        next;
      }
      # Kill random-die objects.
      if(defined $m->{$obj_name}->{_die_chance})
      {
        if(rand(1) < $m->{$obj_name}->{_die_chance})
        {
          delete $m->{$obj_name};
	  next;
        }
      }
      # Advance shrapnel.
      if(defined $m->{$obj_name} and $m->{$obj_name}->isa('Shrapnel'))
      {
        if($obj_name =~ /^P([\-0-9]*),([\-0-9]*)$/)
	{
          $m->{$obj_name}->update_coords($m, $1, $2);
	  next;
	}
      }
      # Advance monsters.
      if(defined $m->{$obj_name} and $m->{$obj_name}->isa('Monster'))
      {
        if($obj_name =~ /^([\-0-9]*),([\-0-9]*)$/)
	{
          $m->{$obj_name}->update_coords($m, $1, $2, $center_x, $center_y);
	  next;
	}
      }
      # Advance bullets.
      if(defined $m->{$obj_name} and $m->{$obj_name}->isa('Bullet'))
      {
        if($obj_name =~ /^([\-0-9]*),([\-0-9]*)$/)
	{
          $m->{$obj_name}->update_coords($m, $1, $2);
	  next;
	}
      }
    }

    # Generate monsters.
    if(rand() < 0.01)
    {
      my ($rand_x, $rand_y) = (int rand MAP_DIM, int rand MAP_DIM);
      if(!defined $m->get_item_at($rand_x, $rand_y))
      {
        my $mon = new Monster("Monster",
                        file_to_string("art/mrrrgle/down"),
                        file_to_string("art/mrrrgle/left"),
                        file_to_string("art/mrrrgle/right"),
                        file_to_string("art/mrrrgle/up"),
			rand()*20, "%", 3, 3,
			rand()*0.3, rand()*0.1
		       );
        $m->set_item_at($rand_x, $rand_y, $mon);
      }
    }

    # Handle movement
    my $in = readkey;
    if($in)
    {
      # Movement
      $m->clear_item_at($center_x,$center_y);
      my ($old_x, $old_y) = ($center_x, $center_y);
      if($in eq 'w') {$center_y--; $player_character->turn_up}
      if($in eq 's') {$center_y++; $player_character->turn_down}
      if($in eq 'a') {$center_x--; $player_character->turn_left}
      if($in eq 'd') {$center_x++; $player_character->turn_right}
      # Check for collision.
      if(defined $m->get_item_at($center_x,$center_y,$player_character))
      {
        ($center_x, $center_y) = ($old_x, $old_y);
      }
      $m->set_item_at($center_x,$center_y,$player_character);
      # Special abilities.
      if($in eq 'l')
      {
        ($center_x, $center_y) = lift($m, $center_x, $center_y);
      }
      my $big = $player_character->get_left() eq $player_character->get_right();
      $big = 0 if $big != 1;
      if($in eq 'j')
      {
        switch($player_character->get_left)
	{
	  case 'S'{($center_x, $center_y) = slash($m,$center_x,$center_y,$big)}
	  case 'R'{($center_x, $center_y) = rocket($m,$center_x,$center_y,$big)}
	  case 'W'{($center_x, $center_y) = ward($m,$center_x,$center_y,$big)}
	}
      }
      if($in eq 'k')
      {
        switch($player_character->get_right)
	{
	  case 'S'{($center_x, $center_y) = slash($m,$center_x,$center_y,$big)}
	  case 'R'{($center_x, $center_y) = rocket($m,$center_x,$center_y,$big)}
	  case 'W'{($center_x, $center_y) = ward($m,$center_x,$center_y,$big)}
	}
      }
    }
    usleep TICK;
  };
}

menu;
