# By Walter Brown.  Happy Birthday Jacob!
package Display;
@ISA = qw(Exporter);
@EXPORT = qw(clear draw_screen set_pixel set_screen get_pixel get_screen
             drop_screen clear_pixel);
use constant SCREEN_HEIGHT => 22;
use constant SCREEN_WIDTH => 78;
use Time::HiRes 'usleep';
use Switch;
use strict;
use warnings;

sub clear
{
  print "\n" x SCREEN_HEIGHT;
}

my %screen = ();

sub get_pixel
{
  my ($x, $y) = @_;
  my $result = $screen{"$x,$y"};
  if(defined $result) {return $result}
  else                {return ' '}
}

# Converts the screen to a string.
sub get_screen
{
  my $image = "\n";
  for(my $y = 0; $y < SCREEN_HEIGHT; $y++)
  {
    $image .= " ";
    for(my $x = 0; $x < SCREEN_WIDTH; $x++)
    {
      if(defined $screen{"$x,$y"})
      {
        $image .= $screen{"$x,$y"};
      }
      else
      {
        $image .= " ";
      }
    }
    $image .= "\n";
  }
  return $image;
}

sub draw_screen
{
  print get_screen;
}

sub set_pixel
{
  my ($x, $y, $set) = @_;
  my $i = $x;
  foreach(split //, $set)
  {
    if($_ eq "\n") {$i = $x; $y++}
    else
    {
      $screen{"$i,$y"} = $_ if $_ ne " ";
      $i++;
    }
  }
}

sub clear_pixel
{
  my ($x, $y) = @_;
  $screen{"$x,$y"} = " ";
}

sub drop_screen
{
  undef %screen;
}

sub set_screen
{
  # Take in new screen image as argument.
  my $new_screen = shift;
  # Drop previous image.
  drop_screen;
  my $x = 0;
  my $y = 0;
  foreach my $l (split //, $new_screen)
  {
    if($l eq "\n")
    {
      # Advance to next line.
      $x = 0;
      $y++;
    }
    else
    {
      # Set this pixel and move onto the next space over.
      set_pixel($x++, $y, $l);
    }
  }
}
1;
